<?php

namespace App\Providers;

use App\Models\Character;
use App\Repositories\CharacterRepository;
use App\Repositories\CharacterRepositoryInterface;
use App\Services\CharacterService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CharacterRepositoryInterface::class, CharacterRepository::class);
        //$this->app->bind(CharacterService::class);
        $this->app->bind(CharacterService::class, function ($app) {
            return new CharacterService($app->make(CharacterRepositoryInterface::class));
        });
    }
}
