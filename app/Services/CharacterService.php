<?php

namespace App\Services;

use App\Repositories\CharacterRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class CharacterService
{
    protected $characterRepository;

    public function __construct(CharacterRepositoryInterface $characterRepository)
    {
        $this->characterRepository = $characterRepository;
    }

    function index($filter) {
        return $this->characterRepository->index($filter);
    }

    function get($id) {
        return $this->characterRepository->get($id);
    }

    function store($attributes) {
        DB::beginTransaction();
        try {
            $character = $this->characterRepository->store($attributes);
        } catch (Exception $exception) {
            DB::rollBack();
        }

        DB::commit();
        return $character;
    }

    function update($id, $attributes) {
        DB::beginTransaction();
        try {
            $character = $this->characterRepository->update($id, $attributes);
        } catch (Exception $exception) {
            DB::rollBack();
        }

        DB::commit();
        return $character;
    }

    function destroy($id) {
        DB::beginTransaction();
        try {
            $this->characterRepository->destroy($id);
        } catch (Exception $exception) {
            DB::rollBack();
        }

        DB::commit();
    }
}
