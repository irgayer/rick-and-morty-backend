<?php

namespace App\Http\Requests\Character;

use Illuminate\Foundation\Http\FormRequest;

class CharacterStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'status' => ['required', 'in:alive,dead'],
            'gender' => ['required', 'in:male,female'],
            'race' => ['required', 'in:human,alien,robot,humanoid,animal'],
            'description' => ['nullable']
        ];
    }
}
