<?php

namespace App\Http\Requests\Character;

use Illuminate\Foundation\Http\FormRequest;

class CharacterUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['nullable'],
            'status' => ['nullable', 'in:alive,dead'],
            'gender' => ['nullable', 'in:male,female'], //transgendering
            'race' => ['nullable', 'in:human,alien,robot,humanoid,animal'],
            'description' => ['nullable']
        ];
    }
}
