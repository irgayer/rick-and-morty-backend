<?php

namespace App\Http\Controllers;

use App\Http\Requests\Character\CharacterStoreRequest;
use App\Http\Requests\Character\CharacterUpdateRequest;
use App\Http\Resources\CharacterResource;
use App\Models\Character;
use App\Services\CharacterService;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    /**
     * @var CharacterService
     */
    private $characterService;

    public function __construct(CharacterService $characterService) {
        $this->characterService = $characterService;
    }

    function index(Request $request) {
        $collection = $this->characterService->index($request->all());

        return CharacterResource::collection($collection);
    }

    function store(CharacterStoreRequest $request) {
        $data = $request->validated();

        $this->characterService->store($data);

        return response()->json(['message' => 'персонаж создан'], 200);
    }

    function find($id) {
        $character = $this->characterService->get($id);

        return new CharacterResource($character);
    }

    function update(CharacterUpdateRequest $request, $id) {
        $data = $request->validated();

        $this->characterService->update($id,$data);

        return response()->json(['message' => 'персонаж изменен'], 200);
    }

    function destroy($id) {
        $this->characterService->destroy($id);

        return response()->json(['message' => 'персонаж удален']);
    }
}
