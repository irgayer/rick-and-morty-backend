<?php

namespace App\Repositories;

interface CharacterRepositoryInterface
{
    public function index($filter);

    public function get($id);

    public function store($attributes);

    public function update($id, $attributes);

    public function destroy($id);
}
