<?php

namespace App\Repositories;

use App\Models\Character;

class CharacterRepository implements CharacterRepositoryInterface
{

    public function index($filter)
    {
        return Character::filter($filter)->paginate($filter['per_page'] ?: 10); //TODO: fix
    }

    public function get($id)
    {
        return Character::where('id', '=', $id)->first();
    }

    public function store($attributes)
    {
        $character = Character::create($attributes);

        return $character->fresh();
    }

    public function update($id, $attributes)
    {
        $character = Character::find($id);
        $character->update($attributes);

        return $character;
    }

    public function destroy($id)
    {
        $character = Character::find($id);

        $character->delete();
    }
}
