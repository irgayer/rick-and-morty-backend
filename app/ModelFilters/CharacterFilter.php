<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class CharacterFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    function name($name) {
        return $this->where('name', 'LIKE', "%$name%");
    }

    function race($races) {
        return $this->whereIn('race', $races);
    }

    function gender($genders) {
        return $this->whereIn('gender', $genders);
    }

    function status($statuses) {
        return $this->whereIn('status', $statuses);
    }
}
