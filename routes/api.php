<?php

use App\Http\Controllers\CharacterController;
use App\Models\Character;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/hello', function () {
    return response()->json('Hello');
});

Route::group([
    'as' => 'characters',
    'prefix' => 'characters'
], function() {
    Route::get('/', [CharacterController::class, 'index']);
    Route::post('/', [CharacterController::class, 'store']);
    Route::put('/{id}/update', [CharacterController::class, 'update']);
    Route::delete('/{id}/delete', [CharacterController::class, 'destroy']);
    Route::get('/{id}', [CharacterController::class, 'find']);
});
